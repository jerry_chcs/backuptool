<?php
/**
 * Simple FTP Class
 * 
 * @package SFTP
 * @name SFTP
 * @version 1.0
 * @author Shay Anderson 05.11
 * @link shayanderson.com
 * @license http://www.gnu.org/licenses/gpl.html GPL License
 * SFTP is free software and is distributed WITHOUT ANY WARRANTY
	* Modified by jerry@chcs.com
 */
final class SFTP {
	/**
	 * FTP host
	 *
	 * @var string $_host
	 */
	private $_host;

	/**
	 * FTP port
	 *
	 * @var int $_port
	 */
	private $_port = 21;

	/**
	 * FTP password
	 *
	 * @var string $_pwd
	 */
	private $_pwd;
	
	/**
	 * FTP stream
	 *
	 * @var resource $_id
	 */
	private $conn_id;

	/**
	 * FTP timeout
	 *
	 * @var int $_timeout
	 */
	private $_timeout = 90;

	/**
	 * FTP user
	 *
	 * @var string $_user
	 */
	private $_user;

	/**
	 * Last error
	 *
	 * @var string $error
	 */
	public $error;

	/**
	 * FTP passive mode flag
	 *
	 * @var bool $passive
	 */
	public $passive = false;

	/**
	 * SSL-FTP connection flag
	 *
	 * @var bool $ssl
	 */
	public $ssl = false;

	/**
	 * System type of FTP server
	 *
	 * @var string $system_type
	 */
	public $system_type;

	/**
	 * Initialize connection params
	 *
	 * @param string $host
	 * @param string $user
	 * @param string $password
	 * @param int $port
	 * @param int $timeout (seconds)
	 */
	public function  __construct($host = NULL, $user = NULL, $password = NULL, $port = 21, $timeout = 90) {
		$this->_host = $host;
		$this->_user = $user;
		$this->_pwd = $password;
		$this->_port = (int)$port;
		$this->_timeout = (int)$timeout;
	}

	/**
	 * Auto close connection
	 */
	public function  __destruct() {
		$this->close();
	}

	/**
	 * Change currect directory on FTP server
	 *
	 * @param string $directory
	 * @return bool
	 */
	public function cd($directory = NULL) {
		// attempt to change directory
		if(ftp_chdir($this->conn_id, $directory)) {
			// success
			return true;
		// fail
		} else {
			$this->error = "Failed to change directory to \"{$directory}\"";
			return false;
		}
	}

	/**
	 * Set file permissions
	 *
	 * @param int $permissions (ex: 0644)
	 * @param string $remote_file
	 * @return false
	 */
	public function chmod($permissions = 0, $remote_file = NULL) {
		// attempt chmod
		if(ftp_chmod($this->conn_id, $permissions, $remote_file)) {
			// success
			return true;
		// failed
		} else {
			$this->error = "Failed to set file permissions for \"{$remote_file}\"";
			return false;
		}
	}

	/**
	 * Close FTP connection
	 */
	public function close() {
		// check for valid FTP stream
		if($this->conn_id) {
			// close FTP connection
			ftp_close($this->conn_id);

			// reset stream
			$this->conn_id = false;
		}
	}

	/**
	 * Connect to FTP server
	 *
	 * @return bool
	 */
	public function connect() {
		// check if non-SSL connection
		if(!$this->ssl) {
			// attempt connection
			if(!$this->conn_id = ftp_connect($this->_host, $this->_port, $this->_timeout)) {
				// set last error
				$this->error = "Failed to connect to {$this->_host}";
				return false;
			}
		// SSL connection
		} elseif(function_exists("ftp_ssl_connect")) {
			// attempt SSL connection
			if(!$this->conn_id = ftp_ssl_connect($this->_host, $this->_port, $this->_timeout)) {
				// set last error
				$this->error = "Failed to connect to {$this->_host} (SSL connection)";
				return false;
			}
		// invalid connection type
		} else {
			$this->error = "Failed to connect to {$this->_host} (invalid connection type)";
			return false;
		}

		// attempt login
		if(ftp_login($this->conn_id, $this->_user, $this->_pwd)) {
			// set passive mode
			ftp_pasv($this->conn_id, (bool)$this->passive);

			// set system type
			$this->system_type = ftp_systype($this->conn_id);

			// connection successful
			return true;
		// login failed
		} else {
			$this->error = "Failed to connect to {$this->_host} (login failed)";
			return false;
		}
	}

	/**
	 * Delete file on FTP server
	 *
	 * @param string $remote_file
	 * @return bool
	 */
	public function delete($remote_file = NULL) {
		// attempt to delete file
		if(ftp_delete($this->conn_id, $remote_file)) {
			// success
			return true;
		// fail
		} else {
			$this->error = "Failed to delete file '$remote_file'";
			return false;
		}
	}

	/**
	 * Download file from server
	 *
	 * @param string $remote_file
	 * @param string $local_file
	 * @param int $mode
	 * @return bool
	 */
	public function get($remote_file = NULL, $local_file = NULL, $mode = FTP_BINARY) {
		// attempt download
		if(ftp_get($this->conn_id, $local_file, $remote_file, $mode)) {
			// success
			return true;
		// download failed
		} else {
			$this->error = "Failed to download file \"{$remote_file}\"";
			return false;
		}
	}

	/**
	 * Get list of files/directories in directory
	 *
	 * @param string $directory
	 * @return array
	 */
	public function ls($directory = NULL) {

		// attempt to get list (returns array on sucess, FALSE on failure
		if (!($list = ftp_nlist($this->conn_id, $directory))) {
			// fail
			$this->error = "Failed to get directory list of '". realpath($directory) . "'";
		}
		return $list;
	}

	/**
	 * Create directory on FTP server
	 *
	 * @param string $directory
	 * @return bool
	 */
	public function mkdir($directory = NULL) {
		// attempt to create dir
		if(ftp_mkdir($this->conn_id, $directory)) {
			// success
			return true;
		// fail
		} else {
			$this->error = "Failed to create directory '$directory'";
			return false;
		}
	}

	/**
	 * Upload file to server
	 *
	 * @param string $local_path
	 * @param string $remote_file_path
	 * @param int $mode
	 * @return bool
	 */
	public function put($local_file = NULL, $remote_file = NULL, $mode = FTP_BINARY) {
		// attempt to upload file
		if(ftp_put($this->conn_id, $remote_file, $local_file, $mode)) {
			// success
			return true;
		// upload failed
		} else {
			$this->error = "Failed to upload file '$local_file'";
			return false;
		}
	}

	/**
	 * Get current directory
	 *
	 * @return string
	 */
	public function pwd() {
		return ftp_pwd($this->conn_id);
	}

	/**
	 * Rename file on FTP server
	 *
	 * @param string $old_name
	 * @param string $new_name
	 * @return bool
	 */
	public function rename($old_name = NULL, $new_name = NULL) {
		// attempt rename
		if(ftp_rename($this->conn_id, $old_name, $new_name)) {
			// success
			return true;
		// fail
		} else {
			$this->error = "Failed to rename file '$old_name'";
			return false;
		}
	}

	/**
	 * Remove directory on FTP server
	 *
	 * @param string $directory
	 * @return bool
	 */
	public function rmdir($directory = NULL) {
		// attempt remove dir
		if(ftp_rmdir($this->conn_id, $directory)) {
			// success
			return true;
		// fail
		} else {
			$this->error = "Failed to remove directory '$directory'";
			return false;
		}
	}
	
	function BuildFileList($dir, $db) {
		$nSkipFiles = array('installFtp.php','installAws.php','class.SFTP.php','dwsync.xml','_notes','error_log');
		
		static $nStart = 0;
		if (!$nStart) {
			$nStart = strlen($dir) + 1;
			// clear dynamic files from directory
			$query = "DELETE FROM tblFilesToCopy WHERE (Static=0)";
			$db->query($query) or die ("Error in query: $query." . $db->error);
		}
		// iterate through all files in site
		$dh  = opendir($dir);
		while (false !== ($filename = readdir($dh))) {
			//echo "$filename\n";	
			if (($filename <> ".") && ($filename <> "..") && !in_array($filename,$nSkipFiles)) {
				//echo "$filename\n";
				$bIsDir = (is_dir("$dir/$filename") ? 1 : 0);
				$BackupKey = 0;
				$fname = substr("$dir/$filename",$nStart);
				$dbfame = str_replace("'","&#039;",$fname);
				$strDir = dirname($dbfame);
				$strFile = basename($dbfame);
				// skip .htaccess & index.php files in tools directory
				if ((($filename == '.htaccess') || ($filename == 'index.php')) && ($strDir == '.')) continue;
				
				$query = "SELECT FileKey FROM tblFilesToCopy WHERE (Directory='$strDir') AND (FileName='$strFile') LIMIT 0,1";
				$result = $db->query($query) or die ("Error in query: $query." . $db->error);
				if (!$result->num_rows) { // no entry present
					// insert into table
					$query = "INSERT INTO tblFilesToCopy ( Directory, FileName, IsDirectory ) VALUES ('$strDir','$strFile', $bIsDir)";
					$db->query($query) or die ("Error in query: $query." . $db->error);
					// echo "$fname added to table<br>";
					//$NewFileCount++;
				}
				$result->free();
				if (is_dir("$dir/$filename")) $this->BuildFileList("$dir/$filename", $db);
			}
		}
	}
	
	function FilesToCopy($db) {
		$query = "SELECT FileKey FROM tblFilesToCopy WHERE (IsDirectory=0)";
		$result = $db->query($query) or die ("Error in query: $query." . $db->error);
		$nCount = $result->num_rows;
		$result->free();
		return $nCount;
	}
	
	function CopyByDirectory($localDir, $remoteDir, $db) {
		$bOK = 1;
		// create directory on remote server
		$strPath = $remoteDir . $localDir;
		$strParent = dirname($strPath);
		$strDir = basename($strPath);
		//echo "<p>$strPath</p>\n";
		if (!@ftp_chdir($this->conn_id, $strPath)){
			if (!ftp_chdir($this->conn_id, $strParent))
			 $bOK = 0;
			elseif (!$this->mkdir($strDir))
				$bOK = 0;
			elseif (!ftp_chdir($this->conn_id, $strDir))
			 $bOK = 0;
		}
		
		if ($bOK) {
			// copy by directory
			$query = "SELECT tblFilesToCopy.Directory, tblFilesToCopy.FileName
					FROM tblFilesToCopy
					WHERE (((tblFilesToCopy.Directory)='$localDir') AND ((tblFilesToCopy.IsDirectory)=0))";
			$result = $db->query($query) or die ("Error in query: $query." . $db->error);
			while ($row = $result->fetch_object()) {
				$strDir = ($localDir == '.' ? '' : "$localDir/");
				//echo "<p>Copy '$row->Directory/$row->FileName' to '$remoteDir$strDir$row->FileName</p>\n";
				if (!$this->put("$row->Directory/$row->FileName",$row->FileName)) {
					$bOK = 0;
					break;
				}
		}
		$result->free();
		}
		return $bOK;
	}
		
}
?>