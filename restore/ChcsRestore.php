<?php
// make sure that user has entered secret
session_start();
if (!@$_SESSION['chcs']['verified']) {
	header("location: ../index.php");
	exit;
}
require('../class.ChcsBackup.php');
$strBackupFilePath = realpath("../ChcsBackup.php");
$objBackup = new ChcsBackup($strBackupFilePath);

use Aws\Common\Enum\Region;
use Aws\Common\Aws;


$cmd = @$_GET['cmd'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CHCS Restore v1.07</title>
<link rel="stylesheet" href="scripts/themes/default/style.min.css" />
<style type="text/css">
<!--
fieldset {
	width:250px;
}
label {
	width:140px;
}
input, select {
	width: 200px;
}
-->
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jstree.min.js"></script>
<script type="text/javascript">

$(function(){ 

 $("#fileList").on('changed.jstree', function (e, data) {
    var i, j, strHtml = '';
    for(i = 0, j = data.selected.length; i < j; i++) {
					 strItem = data.instance.get_node(data.selected[i]).text;
						// <span id="F18|02-Feb-2014_16:46:50">php.ini</span>
						var strFileInfo = strItem.match(/\|[^"]+/ig);
						
						if (strFileInfo) {
							if (strHtml == '') strHtml = 'Restore\n\nFile: ';
							strFileInfo = strFileInfo.toString().replace("_"," ").substr(1);
							aFileInfo = strFileInfo.split('|');
						 strHtml += aFileInfo[1] + "\n\nDate: " + aFileInfo[0] + '?\n';
						}
    }
				if (strHtml) {
					var conf = confirm(strHtml);
					if (conf == true) {
						var data_url = 'RestoreFile.php?path=' + encodeURIComponent(aFileInfo[1]);
						//alert(data_url);
						$("#loading").show();
						$.getJSON(data_url, function(data, textStatus) {
							$("#loading").hide();
							if (textStatus != 'success') alert('Error receiving data:' + textStatus);
							else {
								var len = data.length;
								if (len > 0) {
									for (var i=0; i<len; i++) {
										if ((data[i].key == 'error') && (data[i].value != 0)) alert(data[i].value);
										else alert(data[i].value);
									}
								}
							}
						});
						
					}
				}
  }).jstree({
		"core" : {
    "theme" : {
      "variant" : "large"
				}
		}
 }).fadeIn();
	
	$("#restoreRange").hide();
	$("#loading").fadeOut();
	
	// for command buttons form
	$("#doCmd #update, #doCmd #display").click(function(event){
		$("#cmd").val($(this).attr("id"));
		$("#doCmd").submit();
	});
	
	$("#doCmd #restore").click(function(event){
		$("#commands").fadeOut();
		$("#restoreRange").fadeIn();
	});
	
	$("#doCmd #restoreFiles").click(function(event){
		$("#cmd").val($(this).attr("id"));
		$("#doCmd").submit();
	});
	
	$("#doCmd #cancel").click(function(event){
		$("#restoreRange").fadeOut();
		$("#commands").fadeIn();
	});
	
	$("#doCmd #home").click(function(event){
		window.location = '../index.php';
	});
	
		
});

</script>
</head>

<body>
<?php

$objBackup->CreateBackupDirectory();

// look for aws information file 
$strAwsDataFile = "$objBackup->BACKUP_DIR/aws_config.php";
$bAwsDataComplete = 0;
if (file_exists($strAwsDataFile)) {
	include($strAwsDataFile);
	$bAwsDataComplete = defined('BUCKET') && defined('API_KEY') && defined('SECRET') && BUCKET && API_KEY && SECRET ;
}

if (!$bAwsDataComplete) {
	$submit = @$_POST['formSubmit'];
	if ($submit == 'Save') {
		$BucketName = strtolower(str_replace(array("'",'&',' '),'',@$_POST['BucketName']));
		$ApiKey = @$_POST['ApiKey'];
		$SecretKey = @$_POST['SecretKey'];
		// save information
		$content  = "<?php\n";
		$content .= "define('BUCKET','$BucketName');\n";
		$content .= "define('API_KEY', '$ApiKey');\n";
		$content .= "define('SECRET', '$SecretKey');\n";
		$content .= "?>\n";
		$file = new SplFileObject($strAwsDataFile, "w") ;
		$file->fwrite($content) ;
		unset($file);
		exit;
	}

	$tmStart = strtotime('03:00:00') + rand(0,60 * 60 * 2);
	?>
	<h1>CHCS Restore Configuration</h1>
	<h2>Cron Job Setup</h2>
	<div id="cron">
		<p>Please use the cPanel - Cron Jobs page to add the following Cron Job:</p>
		<p><?php echo (int) date('i',$tmStart) . ' ' . date('G', $tmStart) . '	* 	* 	*  php ' . __FILE__ ?></p>
	</div>
	<h2>AWS Information</h2>
	<form action="<?php  ?>" method="post" enctype="multipart/form-data">
		<p>
			<label for="BucketName">Bucket Name<br>
			</label>
			<input type="text" name="BucketName" id="BucketName" style="text-transform:lowercase;" size="40">
		</p>
		<p>
			<label for="ApiKey">AWS API Key</label><br>
			<input type="text" name="ApiKey" id="ApiKey" size="40">
		</p>
		<p>
			<label for="SecretKey">Secret Key</label><br>
			<input type="text" name="SecretKey" id="SecretKey" size="40">
		</p>
		<p>
			<button type="submit" name="formSubmit" value="Save">Save</button>
		</p>
	</form>
	<?php
	echo "</body>\n</html>\n";
	exit;
}

try {
	// connect to AWS S3
	$awsConfig = array(
		'key' => API_KEY,
		'secret' => SECRET,
		'region' => Region::OREGON);
	// Create a service builder
	$aws = Aws::factory($awsConfig);
	// Get the client from the builder by namespace
	$client = $aws->get('S3');
}
catch(Exception $e) {
	echo "<p>Unable Connect to AWS: " . $e->getMessage() . "</p>\n";
}

?>
 <h2>Restore Utility</h2>
<?php

try {
	// check if BUCKET exists in aws s3
	if ($client->doesBucketExist(BUCKET)) echo "<p>Bucket name: " . BUCKET . "</p>\n";
	else die("<p>" . BUCKET . " not found</p>\n");
}
catch(Exception $e) {
	echo "<p>Unable to find Archive: " . $e->getMessage() . "</p>\n";
	echo "<p><a href=\"" . basename(__FILE__) . "\">Continue</a></p>\n";
	exit;
}

$db = $objBackup->OpenDB();

// create table for restoring files
$query = "CREATE TABLE IF NOT EXISTS `tblChcsRestore` (
	`RestoreKey` int(11) NOT NULL AUTO_INCREMENT,
	`Week` varchar(8) NOT NULL,
	`Path` varchar(255) NOT NULL,
	`FileName` varchar(255) NOT NULL,
	`DTS` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	`Downloaded` tinyint(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`RestoreKey`),
	KEY `FileName` (`FileName`),
	KEY `Week` (`Week`),
	KEY `Downloaded` (`Downloaded`),
	KEY `Path` (`Path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8";
if (!$db->query($query)) die ("Unable to create table: please give user '$strDBUser' CREATE permissions" . $db->error);

// get weeks when backups were made
$aWeek = array();
$query = "SELECT Week FROM tblChcsRestore GROUP BY Week ORDER BY Week";
$result = $db->query($query) or die ("Error in query: $query." . $db->error);
while ($row = $result->fetch_object()) $aWeek[] = $row->Week;
$result->free();

?>
<div>
 <form id="doCmd" action="<?php basename(__FILE__); ?>" method="get" enctype="multipart/form-data">
	 <input type="hidden" name="cmd" id="cmd">
		<div id="commands">
			<button type="button" id="home">Home</button>
			<button type="button" id="update">Update File List</button>
			<button type="button" id="display">Display Most Recent Files</button>
			<button type="button" id="restore">Restore Files</button>
		</div>
		<div id="restoreRange">
		 <fieldset>
			<legend>Select Restore Week Range</legend>
			<p>
				<label for="FirstWeek">First Week: </label>
				<select name="FirstWeek" id="FirstWeek" size="1">
					<?php
					if (sizeof($aWeek)) for ($i=0; $i<sizeof($aWeek); $i++) {
						$strWeek = $objBackup->FormatDateFromDir($aWeek[$i]);
						$selected = (!$i ? 'selected="selected"' : '');
						echo "<option value=\"$aWeek[$i]\" $selected>$strWeek</option>\n";
					}
					?>
				</select>
			</p>
			<p>
				<label for="LastWeek">Last Week: </label>
				<select name="LastWeek" id="LastWeek" size="1">
					<?php
					if (sizeof($aWeek)) for ($i=sizeof($aWeek)-1; $i>=0; $i--) {
						$strWeek = $objBackup->FormatDateFromDir($aWeek[$i]);
						echo "<option value=\"$aWeek[$i]\">$strWeek</option>\n";
					}
					?>
				</select>
			</p>
			<p>
				<label for="Directory">Directory</label>
				<select name="Directory" id="Directory" size="1">
					<?php
					$query = "SELECT tblChcsRestore.Path
						FROM tblChcsRestore
						GROUP BY tblChcsRestore.Path
						ORDER BY tblChcsRestore.Path";
					$result = $db->query($query) or die ("Error in query: $query." . $db->error);
					while ($row = $result->fetch_object()) {
						$strDirectory = ($row->Path ? $row->Path : 'All Directories');
						echo "<option value=\"$row->Path\">$strDirectory</option>\n";
					}
					?>
				</select>
			</p>
			<p>
			 <button type="button" id="restoreFiles">Restore Files</button>
			 <button type="button" id="cancel">Cancel</button>

			</p>
			</fieldset>
			</div>
	</form>
	<p><img src="images/hloading.gif" id="loading"></p>
</div>

<?php

if ($cmd == 'update') {
	echo "<p>Downloading file list from AWS</p>\n";
	//refresh file list
	$query = "TRUNCATE TABLE tblChcsRestore";
	$result = $db->query($query) or die ("Error in query: $query." . $db->error);
	
	$objects = $client->getIterator('ListObjects', array(
		"Bucket" => BUCKET,
		"Prefix" => ""
	)); 
	$query = '';
	$nObjects = 0;
	foreach ($objects as $object) {
		$strKey = $object['Key']; // duplicate single quotes so that they are compatable with mySQL ' delimiter
		$aKey = explode('/',$strKey);
		$nKey = sizeof($aKey);
		$strWeek = $aKey[0]; // first token is Week
		$strFileName = $aKey[$nKey-1]; // last token is filename
		unset($aKey[$nKey-1]); // remove filename
		unset($aKey[0]); // remove Week
		$strPath = implode('/',$aKey);
		$strDate = date('Y-m-d H:i:s', strtotime($object['LastModified']));
		if (($nObjects % 200) == 0) {
			if ($query && ($query != "INSERT INTO tblChcsRestore ( `Week`, Path, FileName, DTS ) VALUES \n")) $db->query(substr($query,0,-1)) or die ("Error in query: $query - " . $db->error);
			$query = "INSERT INTO tblChcsRestore ( `Week`, Path, FileName, DTS ) VALUES \n";
		}
		if (!strpos($strPath,"'") && !strpos($strFileName,"'")) {
			$query .= "( '$strWeek', '$strPath', '$strFileName', '$strDate'),";
	
			//echo $object['Key'] . date('Y-m-d H:i:s', strtotime($object['LastModified'])) . "\n";
			$nObjects++;
		}
	}
	if ($query) $db->query(substr($query,0,-1)) or die ("Error in query: $query." . $db->error);
	echo "<p>$nObjects files found</p>\n";
}

if ($cmd == 'display') {
	echo "<p>&nbsp;</p>\n";
	// put files into nested arrays of directories
	$array = array();
	$query = "SELECT MAX(`Week`) AS `Week`, Path, FileName, MAX(DTS) AS DTS
		FROM tblChcsRestore 
		GROUP BY Path, FileName
		ORDER BY `Week`, FileName";
	$result = $db->query($query) or die ("Error in query: $query." . $db->error);
	while ($row = $result->fetch_object()) {
		$path = trim("$row->Path/$row->FileName", '/');
		$list = explode('/', $path);
		$n = count($list);
		$strDate = date('d-M-Y_H:i:s',strtotime($row->DTS));
		$list[$n-1] = "<span id=\"F|$strDate|$row->Week/" . ($row->Path ? "$row->Path/" : '') . "$row->FileName\">{$list[$n-1]}</span>";
	
		$arrayRef = &$array; // start from the root
		for ($i = 0; $i < $n; $i++) {
			$key = $list[$i];
			$arrayRef = &$arrayRef[$key]; // index into the next level
		}	
	}
	$result->free();
		
	// format output
	function buildUL($array, $prefix) {
			echo "\n<ul>\n";
			foreach ($array as $key => $value) {
					echo "<li>";
					echo "$key";
					// if the value is another array, recursively build the list
					if (is_array($value))
							buildUL($value, "$prefix$key/");
					echo "</li>\n";
			}
			echo "</ul>\n";
	}
	
	echo "<div id=\"fileList\" style=\"display:none;\">\n";
	buildUL($array, '');
	echo "</div>\n";
	echo "<p id=\"event_result\"></p>";
}

if ($cmd == 'restoreFiles') {
	// Get date range
	$FirstWeek = @$_GET['FirstWeek'];
	$LastWeek = @$_GET['LastWeek'];
	$Directory = @$_GET['Directory'];
	if (!$FirstWeek || !is_numeric($FirstWeek) || !$FirstWeek || !is_numeric($FirstWeek)) die("Invalid Week range");
	$strWhere = " AND (`Week` >= '$FirstWeek') AND (`Week` <= '$LastWeek')";
	$nFiles = $nDirs = 0;
	// create directory filter
	$strWhere .= ($Directory ? " AND (tblChcsRestore.Path LIKE '$Directory%')" : '');
	
	// restore directories first
	echo "<p>Restoring directories</p>\n";
	$query = "SELECT tblChcsRestore.Path
		FROM tblChcsRestore
		WHERE (tblChcsRestore.Path != '') AND (tblChcsRestore.Path IS NOT NULL) $strWhere
		GROUP BY tblChcsRestore.Path
		ORDER BY tblChcsRestore.Path";
	$result = $db->query($query) or die ("Error in query: $query." . $db->error);
	while ($row = $result->fetch_object()) {
		$strDirectory = "../../$row->Path";
		if (file_exists($strDirectory)) {} //echo "<p>Directory $strDirectory exists</p>\n";
		elseif (mkdir($strDirectory, 0755, true)) {
			echo "<p>Directory $strDirectory created</p>\n";
			$nDirs++;
		}
		else echo "<p>Unable to create directory $strDirectory</p>\n";
	}
	$result->free();
	echo "<p>$nDirs directories created</p>\n";
	
	// restore files
	echo "<p>Restoring files</p>\n";
	$query = "SELECT MAX(`Week`) AS `Week`, Path, FileName, MAX(DTS) AS DTS
		FROM tblChcsRestore 
		WHERE (Downloaded=0) $strWhere
		GROUP BY Path, FileName
		ORDER BY `Week`, FileName";
	$result = $db->query($query) or die ("Error in query: $query." . $db->error);
	while ($row = $result->fetch_object()) {
		
		$strFileName = "$row->Path/$row->FileName";
		
		if (strpos($strFileName,'backupTool/Aws') !== false) continue;
		if (strpos($strFileName,'backupTool/Chcs') !== false) continue;
		if (strpos($strFileName,'site/config.php') !== false) continue;
		if (strpos($strFileName,'wp/wp-config.php') !== false) continue;
		if (strpos($strFileName,'site/assets/sessions') !== false) continue;
		if (strpos($strFileName,'wp-config.php') !== false) continue;
		if (strpos($strFileName,'backupTool/dbData/aws_config.php') !== false) continue;
		
		$Key = "$row->Week/$strFileName";
		$SaveAs = "../../$strFileName";
		//echo "<p>obj=$Key<br>path=$SaveAs</p>\n";
		// download object to file
		$bOK = 1;
		try {
			$upload = $client->getObject(array(
							'Bucket' => BUCKET,
							'Key'    => $Key,
							'SaveAs' => $SaveAs
			));
		}
		catch(Exception $e) {
			echo "Unable download object AWS: " . $e->getMessage() . "\n";
			$bOK = 0;
		}
		
		if ($bOK) {
			// mark all versions of file as restored
			$query = "UPDATE tblChcsRestore 
				SET Downloaded = 1 
				WHERE (Path='$row->Path') AND (FileName = '$row->FileName')";
			$db->query($query) or die ("Error in query: $query." . $db->error);
			// echo restored file path
			echo "<p>File " . ($bOK ? '' : 'not') . " downloaded: " . @$upload['Body']->getUri() . "</p>\n";
			$nFiles++;
		}
		
		
	}
	$result->free();
	
	echo "<p>Restore complete: $nFiles downloaded</p>\n";
}

$dtEndTime = time();
//echo "Restore Complete: " . date('d-M-Y H:i:s',$dtEndTime) . "\n";
//echo "Elapsed Time: " . (int)($dtEndTime - $dtStartTime) . " Seconds\n\n";

?>

</body>
</html>
