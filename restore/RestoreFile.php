<?php
// make sure that user has entered secret
session_start();
if (!@$_SESSION['chcs']['verified']) {
	header("location: ../index.php");
	exit;
}

require('../class.ChcsBackup.php');
$strBackupFilePath = realpath("../ChcsBackup.php");
$objBackup = new ChcsBackup($strBackupFilePath);

use Aws\Common\Enum\Region;
use Aws\Common\Aws;

$path = @$_GET['path'];

$aPath = explode('/',$path);
if (!is_numeric($aPath[0])) $data[] = array("key" => "error", "value" => "Invalid path: $path");
else {
	unset($aPath[0]);
	$SaveAs = "$objBackup->HOME_DIR/" . implode('/',$aPath);
	
	//$data[] = array("key" => "error", "value" => "Save as: $SaveAs");
	
	$bOK = 1;
	
	// look for aws information file 
	$strAwsDataFile = "$objBackup->BACKUP_DIR/aws_config.php";
	if (file_exists($strAwsDataFile)) {
		include($strAwsDataFile);
		$bOK = defined('BUCKET') && defined('API_KEY') && defined('SECRET') && BUCKET && API_KEY && SECRET ;
	}
	
	if ($bOK) {
		try {
			// connect to AWS S3
			$awsConfig = array(
				'key' => API_KEY,
				'secret' => SECRET,
				'region' => Region::OREGON);
			// Create a service builder
			$aws = Aws::factory($awsConfig);
			// Get the client from the builder by namespace
			$client = $aws->get('S3');
		}
		catch(Exception $e) {
			$data[] = array("key" => "error", "value" => "Unable to Connect to AWS: " . $e->getMessage());
			$bOK = 0;
		}
	}
	
 if ($bOK) {
		// download object to file
		$bOK = 1;
		try {
			$upload = $client->getObject(array(
							'Bucket' => BUCKET,
							'Key'    => $path,
							'SaveAs' => $SaveAs
			));
		}
		catch(Exception $e) {
			$data[] = array("key" => "error", "value" => "Unable download AWS object: " . $e->getMessage());
			$bOK = 0;
		}
	}
	
}

if (!isset($data)) $data[] = array("key" => "msg", "value" => "$SaveAs restored");

echo json_encode($data);

?>