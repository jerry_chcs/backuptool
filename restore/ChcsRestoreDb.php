<?php
// make sure that user has entered secret
session_start();
if (!@$_SESSION['chcs']['verified']) {
	header("location: ../index.php");
	exit;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CHCS Restore Database</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
// this is a "document ready" handler that fires after html is loaded but before window load
$(function(){ 

	$("#buttons #home").click(function(event){
		window.location = '../index.php';
	});


});
</script>
</head>

<body>
<?php

date_default_timezone_set('America/Los_Angeles');
// Define the name of the backup directory
define('BACKUP_DIR', '../dbData' ) ; 

// look for wordpress config file
$configFile = '../../wp/wp-config.php';
if (file_exists($configFile)) {
	$strConfig = file_get_contents($configFile);
	$nMatch = preg_match ( "/define\('DB_NAME', *'([^']+)' *\)/" , $strConfig , $aMatches);
	if ($nMatch) $strDatabase = $aMatches[1]; else echo 'dbName not found';
	$nMatch = preg_match ( "/define\('DB_USER', *'([^']+)' *\)/" , $strConfig , $aMatches);
	if ($nMatch) $strDBUser = $aMatches[1];  else echo 'dbUser not found';
	$nMatch = preg_match ( "/define\('DB_PASSWORD', *'([^']+)' *\)/" , $strConfig , $aMatches);
	if ($nMatch) $strPassword = $aMatches[1]; else echo 'dbPassword not found';
	$nMatch = preg_match ( "/define\('DB_HOST', *'([^']+)' *\)/" , $strConfig , $aMatches);
	if ($nMatch) $strHost = $aMatches[1]; else echo 'dbHost not found';
}
else {
	// look for processwire config file
	$configFile = '../../site/config.php';
	if (file_exists($configFile)) {
		$strConfig = file_get_contents($configFile);
		$nMatch = preg_match ( "/config->dbName *= *'([^']+)'/" , $strConfig , $aMatches);
		if ($nMatch) $strDatabase = $aMatches[1]; else echo 'dbName not found';
		$strConfig = file_get_contents($configFile);
		$nMatch = preg_match ( "/config->dbUser *= *'([^']+)'/" , $strConfig , $aMatches);
		if ($nMatch) $strDBUser = $aMatches[1]; else echo 'dbUser not found';
		$strConfig = file_get_contents($configFile);
		$nMatch = preg_match ( "/config->dbPass *= *'([^']+)'/" , $strConfig , $aMatches);
		if ($nMatch) $strPassword = $aMatches[1]; else echo 'dbPassword not found';
		$strConfig = file_get_contents($configFile);
		$nMatch = preg_match ( "/config->dbHost *= *'([^']+)'/" , $strConfig , $aMatches);
		if ($nMatch) $strHost = $aMatches[1]; else echo 'dbHost not found';
	}
}

if ($strDatabase && $strDBUser && $strPassword && $strHost) {
	// echo @$strDatabase . ' ' . @$strDBUser . ' ' . @$strPassword . ' ' . @$strHost;
	// send to aws
	// sync site with aws
}
else die("Unable to read from config file");
	
// get and echo start time
$dtStartTime = time();
//echo "Starting restore: " . date('d-M-Y H:i:s',$dtStartTime) . $crlf;

//open database connection
$db = new mysqli($strHost , $strDBUser , $strPassword , $strDatabase) ;
if (mysqli_connect_errno()) die("Connect failed: " .  mysqli_connect_error());


function scanForSql($dir) {
	$aFiles = scandir($dir);
	foreach ($aFiles as $filename) {
		if (($filename != '.') && ($filename != '..')) {
			$filePath = "$dir/$filename";
			if (is_dir($filePath)) scanForSql($filePath);
			elseif (substr($filename,-4) == '.sql') echo "<option value=\"$filePath\">$filename</option>\n";
		}
	}
}

?>
<h1>CHCS Restore Database</h1>
<h2>Database: <?php echo $strDatabase; ?></h2>
<div>
	<form name="GetFileName" action="<?php echo basename(__FILE__); ?>" method="post" enctype="multipart/form-data">
		<p>
			<label for="SqlFilePath">Select SQL Backup File to Process: </label>
			<select name="SqlFilePath" id="SqlFilePath" size="1">
				<?php
					// load select in reverse alphabetical order
					scanForSql(realpath(BACKUP_DIR));
				?>
			</select>
		</p>
		<p id="buttons">
			<button type="button" id="home">Home</button>
			<input type="submit" name="submit" value="Continue" />
		</p>
	</form>
</div>

<?php
$nLine = 0;
$SqlFilePath = @$_POST['SqlFilePath'];

if ($SqlFilePath) {
	$query = '';
	$file = new SplFileObject($SqlFilePath, "r");
	
	foreach ($file as $line_num => $line) {
		set_time_limit(20); // allow 20 seconds per query
		
		if (trim($line) == "-- End Query --") {
			// execute query
			//echo "<p>$query</p>\n";
			$query = utf8_encode($query);
			$db->query($query) or die ("Error in query: $query." . $db->error);
			$query = '';
		}
		elseif (substr($line,0,2) != '--') $query .= $line;
		
		
		$nLine++;
	}	
	
	unset($file);
	echo "<p>Restore complete: $nLine lines</p>\n";
	$dtEndTime = time();
	echo "<p>Restore Complete: " . date('d-M-Y H:i:s',$dtEndTime) . "</p>\n";
	echo "<p>Elapsed Time: " . (int)($dtEndTime - $dtStartTime) . " Seconds</p>\n";
}


$db->close();
?>

</body>
</html>
