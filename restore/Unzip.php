<?php
// make sure that user has entered secret
session_start();
if (!@$_SESSION['chcs']['verified']) {
	header("location: ../index.php");
	exit;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Unzip Database Archive</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
// this is a "document ready" handler that fires after html is loaded but before window load
$(function(){ 

	$("#buttons #home").click(function(event){
		window.location = '../index.php';
	});


});
</script>
</head>

<body>
	<h1>CHCS Unzip Database</h1>
	<form name="GetFileName" action="<?php echo basename(__FILE__); ?>" method="post" enctype="multipart/form-data">
		<p>
			<label for="ZipFileName">Select Data File to Process: </label>
			<select name="ZipFileName" id="ZipFileName" size="1">
				<?php
					// load select in reverse alphabetical order
					$strPath = realpath('../dbData');
					$aFiles = scandir($strPath);
					if ($nFiles = sizeof($aFiles))
						for ($i=$nFiles-1; $i>=0; $i--) {
							if (substr($aFiles[$i],-4) == '.zip') {
								$strDate = date('d-M-Y', filemtime($strPath . "/{$aFiles[$i]}"));
								echo "<option value=\"{$aFiles[$i]}\">$strDate {$aFiles[$i]}</option>\br";
							}
					}
				?>
			</select>
		</p>
		<p id="buttons">
			<button type="button" id="home">Home</button>
			<input type="submit" name="submit" value="Continue" />
		</p>
	</form>
 
	<?php
	$aErrorCode[4]  = 'ZIPARCHIVE::ER_SEEK';
	$aErrorCode[5]  = 'ZIPARCHIVE::ER_READ';
	$aErrorCode[9]  = 'ZIPARCHIVE::ER_NOENT';
	$aErrorCode[10] = 'ZIPARCHIVE::ER_EXISTS';
	$aErrorCode[11] = 'ZIPARCHIVE::ER_OPEN';
	$aErrorCode[14] = 'ZIPARCHIVE::ER_MEMORY';
	$aErrorCode[18] = 'ZIPARCHIVE::ER_INVAL';
	$aErrorCode[19] = 'ZIPARCHIVE::ER_NOZIP';
	$aErrorCode[21] = 'ZIPARCHIVE::ER_INCONS';
	
	$ZipFileName = @$_POST['ZipFileName'];
	if ($ZipFileName) {
		$zip = new ZipArchive();
		$res = $zip->open("../dbData/$ZipFileName");
		if ($res === TRUE) {
			$strSaveAs = substr($ZipFileName,0,-4);
			echo "<p>Extracting to $strSaveAs</p>\n";
			$zip->extractTo('../dbdata'); // zip already has the path to the file in it (eg dbData/backup_ddd.sql
			$zip->close();
			echo "<p>Finished</p>\n";
		} else echo "<p>Error: Could not open '$ZipFileName', code: " . @$aErrorCode[$res] . "</p>\n";
	}

	?>	
</body>
</html>