<?php
session_start();
$aError = array();
	
$submit = @$_POST['formSubmit'];

if ($submit) {
	
	$cmd = @$_POST['cmd'];
	$verified = @$_POST['verified'];
	$secret = @$_POST['secret'];
	
	$bOK = false;
	
	if (file_exists('dbData/aws_config.php')) include('dbData/aws_config.php');
	
	$aNoSecretRequired = array('setup_new_site','restore_to_alternate');
	
	if ($cmd == 'setup_new_site')
		$bOK = true;
	else if (!defined('SECRET')) {
		if (in_array($cmd,$aNoSecretRequired)) {
			// no aws_config file required
			$bOK = true;
		}
		else $aError[] = 'AWS Config file missing';
	}
	elseif ($verified && @$_SESSION['chcs']['verified']) {
		// verified
		$bOK = true;
	}
	else {
		// aws_config and verification required
		if (!$secret) $aError[] = 'Please enter a secret';
		elseif ($secret == SECRET) {
			$_SESSION['chcs']['verified'] = 1;
			$bOK = true;
		}
		else $aError[] = 'Secret is not valid';
	}
	
	if ($bOK) {
		
		switch ($cmd) {
			case 'setup_new_site':
				header("location: installAws.php");
				exit;
			case 'restore_from_existing':
				header("location: restore/ChcsRestore.php");
				exit;
			case 'restore_to_alternate':
				break;
			case 'update_api_keys':
				header("location: installAws.php");
				exit;
			case 'database_unzip':
				header("location: restore/Unzip.php");
				exit;
			case 'database_restore':
				header("location: restore/ChcsRestoreDb.php");
				exit;
		}
		
	}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CHCS Backup / Restore Tool</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
$(function(){ 


	$(".help").click(function(e) {
		e.preventDefault();
		$("#error, .help_secret, .help_cron").hide();
		var cmd = $(this).attr("id");
		switch (cmd) {
			case 'help_secret':
				$(".help_secret").slideDown();
				break;
			case 'help_cron':
				$(".help_cron").slideDown();
				break;
		}
	});
	
	$("#setup_new_site").click(function(e) {
		e.preventDefault();
		$("#cmd").val($(this).attr("id"));
		$("#submit").click();
	});


	$(".protected").click(function(e) {
		e.preventDefault();
		$("#error, .help_secret, .help_cron").hide();
		
		var cmd = $(this).attr("id");
		if ($("#verified").val() != '') {
			// secret already entered
			$("#cmd").val(cmd);
			$("#submit").click();
		}
		else {
			// get secret
			$("#cmd").val(cmd);
			$("#action").html($(this).html());
			$("#protected").slideDown();
		}
	});

});
</script>


<style type="text/css">
<!--
nav {
	margin-top:15px;
	margin-bottom:30px;
	display:block;
}

nav ul {
	list-style:none;
	position:relative;
	float:left;
	margin:0;
	padding:0
}

nav ul a {
	display:block;
	color:#333;
	text-decoration:none;
	font-weight:700;
	font-size:12px;
	line-height:32px;
	padding:0 15px;
	font-family:"HelveticaNeue","Helvetica Neue",Helvetica,Arial,sans-serif
}

nav ul li {
	position:relative;
	float:left;
	margin:0;
	padding:0
}

nav ul li.current-menu-item {
	background:#d6d6d6
}

nav ul li:hover {
	background:#d6d6d6;
}

nav ul ul {
	display:none;
	position:absolute;
	top:100%;
	left:0;
	background:#eee;
	padding:0;
	border: 1px solid #bbb;
}

nav ul ul li {
	float:none;
	width:400px
}

nav ul ul a {
	line-height:120%;
	padding:10px 15px
}

nav ul ul ul {
	top:0;
	left:100%
}

nav ul li:hover > ul {
	display:block
}
#protected {
	margin-top:2em;
	display:none;
}
#error,
.form-field {
	margin:1em;
	width:300px;
}
#error fieldset div {
	color:red;
	padding:1em;
}
h1 span {
	font-size:50%;
}
input[type=password] {
	width:100%;
}
#error,
.help_secret, 
.help_cron { 
	display:none;
}
-->
</style>

</head>

<body>
	<h1>CHCS Backup / Restore Tool <span>v0.01</span></h1>
	<nav id="primary_nav">
		<ul>
			<li class="current-menu-item"><a href="index.php">Home</a></li>
			<li><a href="#">Backup</a>
				<ul>
					<li><a href="#" id="setup_new_site">Set up new website</a></li>
					<li><a href="ChcsBackup.php">Backup website</a></li>
				</ul>
			</li>
			<li><a href="#">Restore</a>
				<ul>
					<li><a href="#" class="protected" id="restore_from_existing">From existing backup</a></li>
					<li><a href="#" class="protected" id="restore_to_alternate">From existing backup to alternate location</a></li>
					<li><a href="#" class="protected" id="update_api_keys">Update AWS API keys</a></li>
				</ul>
			</li>
			<li><a href="#">Database</a>
				<ul>
					<li><a href="#" class="protected" id="database_unzip">Unzip database from backup to SQL file</a></li>
					<li><a href="#" class="protected" id="database_restore">Restore database from SQL file</a></li>
				</ul>
			</li>
			<li><a href="#">Help</a>
				<ul>
					<li><a href="#" class="help" id="help_secret">Find your secret</a></li>
					<li><a href="#" class="help" id="help_cron">Setup CRON job for daily backup</a></li>
				</ul>
			</li>
		</ul>
	</nav>
	
	<div style="clear:both"></div>
	
	<div id="error">
		<?php
		if (sizeof($aError)) {
			?>
			<fieldset>
				There are errors:
				<div><?php echo implode('<br />',$aError); ?></div>
			</fieldset>
			<?php 
		}
		?>
	</div>
	
	<div id="protected" <?php if (sizeof($aError)) echo 'style="display:block;"';?>>
		<h2 id="action"></h2>
		<form action="<?php echo basename($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="cmd" id="cmd" value="" />
			<input type="hidden" id="verified" name="verified" value="<?php echo @$_SESSION['chcs']['verified']; ?>">
			<div class="form-field">
			<label>
				Please enter secret:<br />
				<input type="password" name="secret" />
			</label>
			</div>
			<div class="form-field">
				<input type="submit" id="submit" value="Continue" name="formSubmit" />
			</div>
		</form>
	</div>
	
	<div class="help_secret">
		<h2>Find your secret</h2>
		<ol>
			<li>You must have ftp access to your site</li>
			<li>Browse to the backupTool/dbData directory</li>
			<li>The secret is in the aws_config.php file</li>
		</ol>
	</div>
	
	<div class="help_cron">
		<h2>Setup CRON job for daily backup</h2>
		<?php
		/*
		INSERT INTO `tblWebHost` (`WebHostKey`, `WebHostName`, `OffsetInHours`, `CronCommand`, `PhpVersionTool`) VALUES 
		(1,'Hostgator',2,'/opt/php54/bin/php','PHP Configuration'),
		(2,'Arvixe',1,'wget','PHP Version Manager'),
		(3,'Wiredtree',2,'php','PHP Configuration');
		*/
		// calculate a random time between 2am and 4am PT for cron task to run
		?>
		<ol>
			<li>Go to the cpanel for your site at: <?php echo @$_SERVER['SERVER_NAME'] . '/cpanel'; ?></li>
			<li>Enter your user name</li>
			<li>Enter your password</li>
			<li>Make sure you are using PHP version 5.4 or greater</li>
			<li>Use the 'Cron jobs' tool to enter the following Cron Job:
				<ul>
					<li>Wiredtree
						<blockquote>
							<?php 
								$tmCronStart = strtotime('02:00:00') + rand(0,60 * 60 * 2) + (60 * 60 * 2);
								echo (int) date('i',$tmCronStart) . ' ' . date('G', $tmCronStart) . "	* 	* 	*  php " . realpath('ChcsBackup.php'); 
							?>
						</blockquote>
					</li>
					<li>Arvixe VPS
						<blockquote>
							<?php 
								$tmCronStart = strtotime('02:00:00') + rand(0,60 * 60 * 2) + (60 * 60 * 1);
								echo (int) date('i',$tmCronStart) . ' ' . date('G', $tmCronStart) . "	* 	* 	*  php " . realpath('ChcsBackup.php'); 
							?>
						</blockquote>
					</li>
					<li>Arvixe shared hosting
						<blockquote>
							<?php 
								$tmCronStart = strtotime('02:00:00') + rand(0,60 * 60 * 2) + (60 * 60 * 1);
								echo (int) date('i',$tmCronStart) . ' ' . date('G', $tmCronStart) . "	* 	* 	*  wget " . realpath('ChcsBackup.php'); 
							?>
						</blockquote>
					</li>
					<li>Hostgator
						<blockquote>
							<?php 
								$tmCronStart = strtotime('02:00:00') + rand(0,60 * 60 * 2) + (60 * 60 * 2);
								echo (int) date('i',$tmCronStart) . ' ' . date('G', $tmCronStart) . "	* 	* 	*  /opt/php54/bin/php " . realpath('ChcsBackup.php'); 
							?>
						</blockquote>
					</li>
				</ul>
			</li>
		</ol>
	</div>
	
	
</body>
</html>





<?php
//header("Location: installAws.php");
?>