<?php

class log extends SplFileObject {
	
	public function fwrite($str, $length = NULL) {
		SplFileObject::fwrite(date('Y-m-d H:i:s ') . "$str\n");
	}
	
}

require('class.ChcsBackup.php');
$objBackup = new ChcsBackup();

$log = new log("$objBackup->PROGRAM_DIR/BackupLog.txt", "w"); 
$log->fwrite("Starting program");

$log->fwrite("Created ChcsBackup class");

use Aws\Common\Enum\Region;
use Aws\Common\Aws;

$log->fwrite("After use Aws...");

$objBackup->CreateBackupDirectory();

$log->fwrite("Checked for / Created backup directory");

// look for aws information file 
$strAwsDataFile = "$objBackup->BACKUP_DIR/aws_config.php";
$bAwsDataComplete = 0;
if (file_exists($strAwsDataFile)) {
	include($strAwsDataFile);
	$bAwsDataComplete = defined('BUCKET') && defined('API_KEY') && defined('SECRET') && BUCKET && API_KEY && SECRET ;
}

$log->fwrite("Checked for Aws data file");

if (!$bAwsDataComplete) {
	$submit = @$_POST['formSubmit'];
	if ($submit == 'Save') {
		$BucketName = strtolower(str_replace(array("'",'&',' '),'',@$_POST['BucketName']));
		$ApiKey = @$_POST['ApiKey'];
		$SecretKey = @$_POST['SecretKey'];
		// save information
		$content  = "<?php\n";
		$content .= "define('BUCKET','$BucketName');\n";
		$content .= "define('API_KEY', '$ApiKey');\n";
		$content .= "define('SECRET', '$SecretKey');\n";
		$content .= "?>\n";
		$file = new SplFileObject($strAwsDataFile, "w") ;
		$file->fwrite($content) ;
		unset($file);
		die("<a href=\"" . basename(__FILE__) . "\">Continue</a>\n");
	}
	?>
	<!doctype html>
	<html>
	<head>
	<meta charset="utf-8">
	<title>CHCS Backup Configuration</title>
	</head>
	
	<body>
	<?php
		$tmStart = strtotime('03:00:00') + rand(0,60 * 60 * 2);
	?>
	<h1>CHCS Backup Configuration</h1>
	<h2>Cron Job Setup</h2>
	<div id="cron">
		<p>Please use the cPanel - Cron Jobs page to add the following Cron Job:</p>
		<p><?php echo (int) date('i',$tmStart) . ' ' . date('G', $tmStart) . '	* 	* 	*  php ' . __FILE__ ?></p>
	</div>
	<h2>AWS Information</h2>
	<form action="<?php  ?>" method="post" enctype="multipart/form-data">
		<p>
			<label for="BucketName">Bucket Name<br>
			</label>
			<input type="text" name="BucketName" id="BucketName" style="text-transform:lowercase;" size="40">
		</p>
		<p>
			<label for="ApiKey">AWS API Key</label><br>
			<input type="text" name="ApiKey" id="ApiKey" size="40">
		</p>
		<p>
			<label for="SecretKey">Secret Key</label><br>
			<input type="text" name="SecretKey" id="SecretKey" size="40">
		</p>
		<p>
			<button type="submit" name="formSubmit" value="Save">Save</button>
		</p>
	</form>
	</body>
	</html>
	<?php
	exit;
}

try {
	// connect to AWS S3
	$awsConfig = array(
		'key' => API_KEY,
		'secret' => SECRET,
		'region' => Region::OREGON);
	// Create a service builder
	$aws = Aws::factory($awsConfig);
	// Get the client from the builder by namespace
	$client = $aws->get('S3');
}
catch(Exception $e) {
	echo "<p>Unable Connect to AWS: " . $e->getMessage() . "</p>\n";
}

$log->fwrite("Connected to Aws");

try {
	// check if BUCKET exists in aws s3
	if ($client->doesBucketExist(BUCKET)) echo "Bucket name: " . BUCKET . "\n";
	else die("<p>" . BUCKET . " not found</p>\n");
}
catch(Exception $e) {
	echo "<p>Unable to find Archive: " . $e->getMessage() . "</p>\n";
	echo "<p><a href=\"" . basename(__FILE__) . "\">Continue</a></p>\n";
	exit;
}

$log->fwrite("Bucket" . BUCKET . " found in Aws");

// db backup needs delimiter array
$aNeedsDelimiter['bigint'] = false;
$aNeedsDelimiter['binary'] = false;
$aNeedsDelimiter['bit'] = false;
$aNeedsDelimiter['blob'] = false;
$aNeedsDelimiter['bool'] = false;
$aNeedsDelimiter['char'] = true;
$aNeedsDelimiter['date'] = true;
$aNeedsDelimiter['datetime'] = true;
$aNeedsDelimiter['decimal'] = false;
$aNeedsDelimiter['double'] = false;
$aNeedsDelimiter['enum'] = true;
$aNeedsDelimiter['float'] = false;
$aNeedsDelimiter['int'] = false;
$aNeedsDelimiter['longblob'] = false;
$aNeedsDelimiter['longtext'] = true;
$aNeedsDelimiter['mediumblob'] = false;
$aNeedsDelimiter['mediumint'] = false;
$aNeedsDelimiter['mediumtext'] = true;
$aNeedsDelimiter['set'] = true;
$aNeedsDelimiter['smallint'] = false;
$aNeedsDelimiter['text'] = true;
$aNeedsDelimiter['time'] = true;
$aNeedsDelimiter['timestamp'] = true;
$aNeedsDelimiter['tinyblob'] = false;
$aNeedsDelimiter['tinyint'] = false;
$aNeedsDelimiter['tinytext'] = true;
$aNeedsDelimiter['varbinary'] = true;
$aNeedsDelimiter['varchar'] = true;
$aNeedsDelimiter['year'] = false;

$log->fwrite("Looking for database config file");

$db = $objBackup->OpenDB();

$log->fwrite("Opened database connection");
	
// begin export of db backup
// Define the filename for the sql file
// Amazon's S3 service only allows lower-case letters 
// Data backed up before 6::am reflects previous day's data
$fileName = strtolower('backup_' . date('D',strtotime('-6 hours')) . '.sql'); 
// Set execution time limit
set_time_limit(300);

// get and echo start time
$dtStartTime = time();
echo "Starting " . (isset($_GET['reset']) ? "reset: " : "export: ") . date('d-M-Y H:i:s',$dtStartTime) . "\n";


$log->fwrite("Creating / Checking for tblChcsBackup");

// create tblBackup if it doesn't exist
$query = "CREATE TABLE IF NOT EXISTS `tblChcsBackup` (
	`BackupKey` int(11) NOT NULL AUTO_INCREMENT,
	`FileName` varchar(255) NOT NULL,
	`DTS` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`BackupKey`),
	KEY `FileName` (`FileName`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8";
if (!$db->query($query)) die ("Unable to create table: please give user '$strDBUser' CREATE permissions" . $db->error);

// back up database as a set of sql statements
if (!isset($_GET['sync']) && !isset($_GET['reset'])) { 

 $log->fwrite("Starting database backup");

	// open temp file
	$strFileName = "$objBackup->BACKUP_DIR/$fileName";
	try {
		$file = new SplFileObject($strFileName, "w");
		echo "Opened temp file: $strFileName\n";
	}
	catch(Exception $e) {
		echo "Unable to open temp file: $strFileName: " . $e->getMessage() . "\n";
	}

	// Backup header information
	$file->fwrite("-- ---------------------------------------------------\n");
	$file->fwrite("--\n");
	$file->fwrite("-- CHCS mySQL Database Backup System \n");
	$file->fwrite("--\n");
	$file->fwrite('-- Export created: ' . date("d-M-Y H:i:s") . "\n");
	$file->fwrite("--\n");
	$file->fwrite("-- Database : $strDatabase\n");
	$file->fwrite("--\n");
	$file->fwrite("-- ---------------------------------------------------\n");
	$file->fwrite("SET AUTOCOMMIT = 0;\n");
 $file->fwrite("-- End Query --\n");
	$file->fwrite("SET FOREIGN_KEY_CHECKS=0;\n");
 $file->fwrite("-- End Query --\n");
	
	$aSkipTable = array();
	
	$resultTable = $db->query("SHOW TABLES");
	echo "Tables to Export: " . ($resultTable->num_rows - sizeof($aSkipTable)) . "\n";
	$nTablesExported = $nTablesSkipped = $nTablesProcessed = 0;
	// export each table schema and data
	while ($rowTable = $resultTable->fetch_row()) { 
		$TableName =  $rowTable[0];
		if (!$TableName || in_array($TableName, $aSkipTable) || (substr($TableName,0,3) == 'tmp')) {
			echo "$TableName Skipped: Temporary Table\n";
			$nTablesSkipped++;
			continue;
		}
		
  $log->fwrite("Backing up table '$TableName'");

		// Get the table-schema
		$schema = $db->query("SHOW CREATE TABLE $TableName") ;
		$tableschema = $schema->fetch_row() ;
		// modify create sql so table is only created if it does not exist
		$tableCreate = str_replace('CREATE TABLE','CREATE TABLE IF NOT EXISTS',$tableschema[1]);
		$file->fwrite($tableCreate . ";\n\n"); 
		$schema->free();
		$file->fwrite("-- End Query --\n");
		
		// get information about each field in table
		$resultField = $db->query("SHOW COLUMNS FROM `$TableName`");
		$Fieldlist = "";
		$FieldCount = 0;
		$bFirstField = 1;
		if (isset($aFieldDelimiter)) unset($aFieldDelimiter);
		if (isset($aFieldNull)) unset($aFieldNull);
		while ($rowField = $resultField->fetch_object()) {
			$Fieldlist .= ($bFirstField ? "" : ", ") . "`$rowField->Field`";
			//remove anythig in parens after type - e.g. int(11)
			$tmpType = $rowField->Type;
			$nPos = strpos($tmpType,'(');
			if ($nPos > 0) $tmpType = substr($tmpType,0,$nPos);
			$aFieldDelimiter[] = $aNeedsDelimiter[$tmpType];
			$aFieldNull[] = ($rowField->Null == 'Yes');
			$aFieldDefault[] = $rowField->Default;
			
			$bFirstField = 0;
			$PrimaryKey = '';
			if ($rowField->Key == "PRI") { $PrimaryKey = $rowField->Field; }
			$FieldCount++;
		}
		$resultField->free();
		// write out insert queries
		$SelectQuery = "SELECT $Fieldlist FROM $TableName " . ($PrimaryKey ? "ORDER BY $PrimaryKey" : '');
		$result = $db->query($SelectQuery) or die("Query failed: $SelectQuery");
		$bFirstRow = 1;
		if ($result->num_rows) {
			$nTablesExported++;
			$file->fwrite("TRUNCATE TABLE $TableName;\n");
			$nRowCount = 0;
			while ($row = $result->fetch_row()) {
				if (($nRowCount++ % 100) == 0) {
					if (!$bFirstRow) $file->fwrite(";\n");
					$file->fwrite("-- End Query --\n");
					$file->fwrite("INSERT INTO $TableName ($Fieldlist) VALUES");
					$bFirstRow = 1;
				}
				$file->fwrite(($bFirstRow ? "" : ",") . "\n(");
				$bFirstField = 1;
				for ($i = 0 ; $i < $FieldCount ; $i++) {
					$delim = ($aFieldDelimiter[$i] ? "'" : '');
					$value = ($aFieldNull[$i] ? 'NULL' : str_replace("'", "&#039;", $row[$i]));
					if ($value == '') $value = 'NULL';
					if ($value == 'NULL') $delim = '';
					$file->fwrite(($bFirstField ? "" : ", ") . "$delim$value$delim");
					$bFirstField = 0;
				}
				$file->fwrite(")");
				$bFirstRow = 0;
			}
			$file->fwrite(";\n-- End Query --\n");
		} 
		else { 
			echo "$TableName Skipped: No Data\n";
			$nTablesSkipped++;
		}
		$result->free();
	}
	
	$file->fwrite('SET FOREIGN_KEY_CHECKS = 1 ; '  . "\n"); 
 $file->fwrite("-- End Query --\n");
	$file->fwrite('COMMIT ; '  . "\n");
 $file->fwrite("-- End Query --\n");
	$file->fwrite('SET AUTOCOMMIT = 1 ; ' . "\n"); 
 $file->fwrite("-- End Query --\n");
	unset($file); // destroying class closes file
	
	$log->fwrite("Zipping database backup");

	
	if (file_exists($strFileName)) {
		$nFileSize = $objBackup->FormatFileSize(filesize($strFileName)); 
		echo "Temp file: '$strFileName' verified ($nFileSize)\n";
		$nFileSize = 0;
		
		echo "Tables Skipped: $nTablesSkipped\n";
		echo "Tables Exported: $nTablesExported\n";
	
		// convert the backup into a zip file to save space
		$strArchiveFileName = "$objBackup->BACKUP_DIR/$fileName.zip";
		// if the file already exists, remove it
		if (file_exists($strArchiveFileName)) unlink($strArchiveFileName);
		
		$zip = new ZipArchive();
		$resOpen = $zip->open($strArchiveFileName , ZIPARCHIVE::CREATE) ;
		if ($resOpen) {
			$zip->addFile($strFileName) ;
			$bZipSaved = $zip->close();
		}
		$nFileSize = $objBackup->FormatFileSize(filesize($strArchiveFileName)) ; 
		
		// remove temp file
		unlink($strFileName);
		
  $log->fwrite("Database backup zipped");
		
	}
	
	if (@$nFileSize && @$bZipSaved) {
		echo "Backup file: $strArchiveFileName ($nFileSize)\n";
		$dtEndTime = time();
		echo "Export Complete: " . date('d-M-Y H:i:s',$dtEndTime) . "\n";
		echo "Elapsed Time: " . (int)($dtEndTime - $dtStartTime) . " Seconds\n\n";
	}
	else echo "*** BACKUP FAILED ***\n";
} // end of db backup to zip file

$log->fwrite("Starting File Archive");

//$tmDate = time();
//$nDayOfWeek = date('w',$tmDate);
//$tmStartOfWeek = $tmDate - ($nDayOfWeek * 60 * 60 * 24);

// backups created before 6:00am reflect previous day's data
$ArchiveDirectory = date('Ymd',strtotime('-6 hours'));
$NewFileCount = $BackupFileCount = 0;

function ArchiveFiles($dir) {
	global $NewFileCount, $BackupFileCount, $db, $client, $ArchiveDirectory, $log;
	static $nStart = 0;
	if (!$nStart) $nStart = strlen($dir) + 1;
	
	$aSkipDir = array('_notes','page_enhanced','data','Aws','cache','sessions');
	$aSkipFile = array('error_log','BackupLog.txt');
	
	// iterate through all files in site
	$dh  = opendir($dir);
	while (false !== ($filename = readdir($dh))) {
		//echo "$filename\n";	
		if (($filename <> ".") && ($filename <> "..") && (!in_array($filename,$aSkipFile))) {
			if (is_dir("$dir/$filename")) {
				//echo "Directory: $dir/$filename\n";
				if (!in_array($filename,$aSkipDir)) ArchiveFiles("$dir/$filename");
			}
			else {
				//echo "$filename\n";
				$BackupKey = 0;
				$dts = '0000-00-00 00:00:00';
				$fname = substr("$dir/$filename",$nStart);
				$dbfame = str_replace("'","&#039;",$fname);
				$dtModified = date('Y-m-d H:i:s', filemtime("$dir/$filename"));
				//echo "$fname $dtModified\n";
				$query = "SELECT BackupKey, DTS FROM tblChcsBackup WHERE FileName='$dbfame' LIMIT 0,1";
				$result = $db->query($query) or die ("Error in query: $query." . $db->error);
				if (!$result->num_rows) { // no entry present
					// insert into table
					$query = "INSERT INTO tblChcsBackup ( FileName ) VALUES ('$dbfame')";
					$db->query($query) or die ("Error in query: $query." . $db->error);
					$BackupKey = $db->insert_id;
					// echo "$fname added to table<br>";
					$NewFileCount++;
				}
				elseif ($row = $result->fetch_object()) {
					$BackupKey = $row->BackupKey;
					$dts = $row->DTS;
				}
				$result->free();
				if ($dtModified != $dts) {
					if (isset($_GET['reset'])) $bError = 0;
					else {
						// backup file
						if (!$BackupFileCount) echo "Backing up file(s):\n";
						echo " $fname\n";
						
  				$log->fwrite("Backing up file '$fname'");
						
						$bError = 0;
						try {
							$result = $client->putObject(array(
								'Bucket'     => BUCKET,
								'Key'        => "$ArchiveDirectory/$fname",
								'SourceFile' => realpath("$dir/$filename"),
							));
						}
						catch (Exception $e) {
							echo "Error uploading: $fname - " . $e->getMessage() . "\n";
							$bError = 1;
						}
					}
					if (!$bError && $BackupKey) {
						// update db DTS
						$query = "UPDATE tblChcsBackup SET DTS = '$dtModified' WHERE (BackupKey=$BackupKey)";
						$db->query($query) or die ("Error in query: $query." . $db->error);
						$BackupFileCount++;
					}
				}
			}
		}
	}
}

ArchiveFiles($objBackup->HOME_DIR);

echo ($NewFileCount ? "$NewFileCount new files found" : "No new files were found") . "\n";
if (isset($_GET['reset']))
 echo ($BackupFileCount ? "$BackupFileCount file records reset" : "No new file records were reset") . "\n";
else
	echo ($BackupFileCount ? "$BackupFileCount files added to archive" : "No new files were backed up") . "\n";


$dtEndTime = time();
echo (isset($_GET['reset']) ? "Reset complete: " : "Synchronizing Complete: ") . date('d-M-Y H:i:s',$dtEndTime) . "\n";
echo "Elapsed Time: " . (int)($dtEndTime - $dtStartTime) . " Seconds\n\n";

$log->fwrite("Complete");

$log->fflush();

// destroy ChcsBackup object 
unset($objBackup);
?>
