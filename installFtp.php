<?php
session_start();

$bError = 0;

$submit = @$_POST['formSubmit'];
if ($submit) {
	$HostName = trim(@$_POST['HostName']);
	$UserName = trim(@$_POST['UserName']);
	$Password = trim(@$_POST['Password']);
	$URL = trim(@$_POST['URL']);
	if ($URL) {
		if (substr($URL,-1,1) != '/') $URL .= '/';
		if (substr($URL,0,4) != 'http') $URL = "http://$URL";
	}
	$WebHostKey = trim(@$_POST['WebHostKey']);
	if (isset($_POST['bError'])) {
		$bError = 1; // set error flag
		$submit = 0; // show form to make corrections
	}
	if (!$HostName) { $bError = 1; $_SESSION['error'][] = "Please enter the FTP Hostname"; }
	if (!$UserName) { $bError = 1; $_SESSION['error'][] = "Please enter the FTP UserName"; }
	if (!$Password) { $bError = 1; $_SESSION['error'][] = "Please enter the FTP Password"; }
	if (!$URL) { $bError = 1; $_SESSION['error'][] = "Please enter the Website URL"; }
	if (!$WebHostKey) { $bError = 1; $_SESSION['error'][] = "Please enter the Website Host"; }
}
else {
		//$HostName = '192.185.26.252';
		//$UserName = 'ljharris';
		//$Password= '0lgusY58qJtM';
}
//opend database connection
$db = new mysqli('localhost' , 'chcsweb_redtruck' , 'w;wV-au^%X~P' , 'chcsweb_backupTool') ;
if (mysqli_connect_errno()) die("Connect failed: " .  mysqli_connect_error());
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Backup Tool Installer</title>
<style type="text/css">
<!--
input, select {
	width:230px;
}
label {
	display:block;
}
.error {
	color:red; 
	font-weight:bold;
}
-->
</style>

</head>

<body>
 <h2>Backup Tool Installer (Step 2 of 2)</h2>
	<?php 
	if (!$submit || $bError) { 
	?>
	<h3>Please enter information on the site where you are installing</h3>
 <form action="<?php basename(__FILE__); ?>" method="post" enctype="multipart/form-data">
		<!-- ask for host,user,password,site URL and host site name -->
	 <?php 
		 if ($bError && isset($_SESSION['error'])) {
				echo "<ul style=\"color:red; font-weight:bold;\">\n";
				foreach ($_SESSION['error'] as $msg) echo "<li>$msg</li>\n";
				echo "</ul>\n"; 
			}
			unset($_SESSION['error']);
		?>
		<p>
		 <label for="HostName">FTP Hostname</label>
			<input type="text" name="HostName" id="HostName" value="<?php echo @$HostName; ?>">
		</p>
		<p>
		 <label for="UserName">FTP UserName</label>
			<input type="text" name="UserName" id="UserName" value="<?php echo @$UserName; ?>">
		</p>
		<p>
		 <label for="Password">FTP Password</label>
			<input type="password" name="Password" id="Password" value="<?php echo @$Password; ?>">
		</p>
		<p>
		 <label for="URL">Website URL</label>
			<input type="text" name="URL" id="URL" value="<?php echo @$URL; ?>">
		</p>
		<p>
		 <label for="WebHostKey">Website Host</label>
			<select name="WebHostKey" id="WebHostKey" size="1">
				<option value="0">Select Website Host</option>
				<?php
				$query = "SELECT WebHostKey, WebHostName FROM tblWebHost ORDER BY WebHostName";
				$result = $db->query($query) or die ("Error in query: $query." . mysql_error());
				while ($row = $result->fetch_object()) {
					$selected = (@$WebHostKey == $row->WebHostKey ? 'selected="selected"' : '');
					echo "<option value=\"$row->WebHostKey\" $selected>$row->WebHostName</option>\n";
				}
				$result->free();
				?>
			</select>
		</p>
		<p>
			<button type="submit" name="formSubmit" value="submit">Continue</button>
		</p>
	</form>


 <?php
	}
	else {
			
		$tmStart = time();
		
		include('class.SFTP.php');
		
		$ftp = new SFTP($HostName, $UserName, $Password); 
		
			// use passive mode
			$ftp->passive = true;
		
		if ($ftp->connect()) { 
		
			echo "<p>Connected to '$HostName'</p>\n";
		
			// cd to public_html
			if (!$ftp->cd("/public_html")) {
				$bError = 1;
				$_SESSION['error'][] = $ftp->error;
				$_SESSION['error'][] = "FTP Credentials did not connect to site's root directory";
			}
				
				
			
			// check if backupTool directory exists
			if (!$bError) {
				$contents = $ftp->ls('.');
			 if (($contents == FALSE) || !is_array($contents)) {
					$bError = 1;
					$_SESSION['error'][] = $ftp->error;
				 $_SESSION['error'][] = "Error checking for 'backupTool' directory";
				}
			}
			
			if (!$bError) {
			 if (!is_array($contents) || !in_array('backupTool',$contents)) {
					if (!$ftp->mkdir('backupTool')) {
						$bError = 1;
						$_SESSION['error'][] = $ftp->error;
					}
				}
			}
			
			if (!$bError) {
				if (!$ftp->cd("/public_html/backupTool")) {
					$bError = 1;
					$_SESSION['error'][] = $ftp->error;
				}
			}
			
			// print current directory
			echo "<p>Directory: " . $ftp->pwd() . "</p>\n";
			
			/* get contents of the current directory
		  $contents = $ftp->ls(".");
			
			// output $contents
			if (isset($contents) && is_array($contents)) 
				foreach ($contents as $fileName) {
					echo "$fileName<br>\n";
				}
			*/
			
			
			$ftp->BuildFileList(realpath('.'), $db);
			
			echo "<p>Copying " . $ftp->FilesToCopy($db) . " files to website ...</p>";
			
				// copy by directory
			$query = "SELECT tblFilesToCopy.Directory
				FROM tblFilesToCopy
				GROUP BY tblFilesToCopy.Directory
				ORDER BY tblFilesToCopy.Directory";
			$result = $db->query($query) or die ("Error in query: $query." . $db->error);
			while ($row = $result->fetch_object()) {
				if (!$ftp->CopyByDirectory($row->Directory,"/public_html/backupTool/",$db)) {
					$bError = 1;
					$_SESSION['error'][] = $ftp->error;
					$_SESSION['error'][] = "Unable to copy all files to '$HostName'";
					break;
				};
			}
			$result->free();
		
		}
		else {
			$_SESSION['error'][] = "Unable to connect to '$HostName'";
			$bError = 1;
		}
		
		$tmEnd = time();
		
		echo "<p>Elapsed time: " . (int) ($tmEnd - $tmStart) . " seconds</p>\n";
		
		if (!$bError) {
			echo "<p>Installation complete - please see instructions below to set PHP version and a Cron task on site</p>";
			// get information about host
			$query = "SELECT WebHostName, OffsetInHours, CronCommand, PhpVersionTool
				FROM tblWebHost
				WHERE (WebHostKey=$WebHostKey)";
			$result = $db->query($query) or die ("Error in query: $query." . $db->error);
			if ($row = $result->fetch_object()) {
				$WebHostName = $row->WebHostName;
				$OffsetInHours = $row->OffsetInHours;
				$CronCommand = $row->CronCommand;				
				$PhpVersionTool = $row->PhpVersionTool;
			}
			$result->free();
			// calculate a random time between 2am and 4am PT for cron task to run
		 $tmCronStart = strtotime('02:00:00') + rand(0,60 * 60 * 2) + (60 * 60 * $OffsetInHours);
			?>
			<ol>
				<li>Go to the cpanel for your site at: <?php echo $URL . 'cpanel'; ?></li>
				<li>Enter your user name as: <?php echo $UserName; ?></li>
				<li>Enter your password as: <?php echo $Password; ?></li>
				<li>Use the '<?php echo $PhpVersionTool; ?>' tool to set the PHP version to 5.4</li>
				<li>Use the 'Cron jobs' tool to enter the following Cron Job:
					<ul>
						<li>
							<?php 
								echo (int) date('i',$tmCronStart) . ' ' . date('G', $tmCronStart) . "	* 	* 	*  $CronCommand " . 
									($CronCommand != 'wget' ? '&lt;Home Directory&gt;/public_html/' : $URL) . 'backupTool/ChcsBackup.php '; 
							?>
						</li>
						<?php if ($CronCommand != 'wget') { ?>
						<li>Look in cPanel (on the main page, on the left hand side under Stats) and substitute the path shown 
							in 'Home Directory' for &lt;Home Directory&gt; in the cron job above 
							(e.g. /home/chcs/public_html/backupTool/ChcsBackup.php)</li>
						<?php } ?>
					</ul>
				</li>
			</ol>
			<?php
		}
		else {
			?>
			<form action="<?php basename(__FILE__); ?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="bError" value="1">
			 <input type="hidden" name="HostName" value="<?php echo $HostName; ?>">
			 <input type="hidden" name="UserName" value="<?php echo $UserName; ?>">
			 <input type="hidden" name="Password" value="<?php echo $Password; ?>">
			 <input type="hidden" name="URL" value="<?php echo $URL; ?>">
			 <input type="hidden" name="WebHostKey" value="<?php echo $WebHostKey; ?>">
			 <p class="error">There were error(s) encountered</p>
				<p><button name="formSubmit" type="submit" value="submit">Continue and fix error(s)</button></p>
			</form>
			<?php
		}
		
	}
 $db->close();
?>

</body>
</html>