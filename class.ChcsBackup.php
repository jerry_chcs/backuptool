<?php
// -------------------------------------------------------
// class declaration for ChcsBackup
// -------------------------------------------------------

require 'Aws/aws-autoloader.php';

use Aws\Common\Enum\Region;
use Aws\Common\Aws;


if (!class_exists('ChcsBackup', false)) {

	class ChcsBackup {
		
		public $db = NULL;
		
		public $BACKUP_DIR = NULL;
		
		public $HOME_DIR = NULL;
		
		public $PROGRAM_DIR = NULL;
		
		public function __construct() {
			date_default_timezone_set('America/Los_Angeles');
			$this->PROGRAM_DIR = dirname(__FILE__);
			if (!$this->PROGRAM_DIR) $this->PROGRAM_DIR = dirname(realpath(__FILE__));
			$this->BACKUP_DIR = "$this->PROGRAM_DIR/dbData";
			$this->HOME_DIR = dirname($this->PROGRAM_DIR);
		}
		
		public function __destruct() {
			if ($this->db !== NULL) {
				$this->db->close();
				$this->db = null;
			}
		}
		
		public function FormatFileSize($file_size) {
			// Formats and appends proper Unit after file_size
			$retval = '';
			if ($file_size/1024 < 1) 
					$retval = intval($file_size) ." Bytes";
			elseif ($file_size/1024 >= 1 && $file_size/(1024*1024) < 1)
				$retval =  intval($file_size/1024) ." KB";
			else
				$retval = intval($file_size / (1024*1024)) . " MB";
			return $retval;
		}
		
		public function CreateBackupDirectory() {
			// Check if dbData directory is already created and has the proper permissions
			if (!file_exists($this->BACKUP_DIR)) mkdir($this->BACKUP_DIR , 0700) ;
			if (!is_writable($this->BACKUP_DIR)) chmod($this->BACKUP_DIR , 0700) ; 
			
			// Create an ".htaccess" file if not present, it will restrict direct accss to the backup-directory. 
			$strHtaccessFile = "$this->BACKUP_DIR/.htaccess";
			if (!file_exists($strHtaccessFile)) {
				$content = 'deny from all'; 
				$file = new SplFileObject($strHtaccessFile, "w");
				$file->fwrite($content);
				unset($file);
			}
		}
		
		public function OpenDB() {
			// look for wordpress config file
			$configFile = "$this->HOME_DIR/wp/wp-config.php";
			$bFound = file_exists($configFile);
			if (!$bFound) {
				$configFile = "$this->HOME_DIR/wp-config.php";
				$bFound = file_exists($configFile);
			}
			if ($bFound) {
				$strConfig = file_get_contents($configFile);
				$nMatch = preg_match ( "/define\('DB_NAME', *'([^']+)' *\)/" , $strConfig , $aMatches);
				if ($nMatch) $strDatabase = $aMatches[1]; else echo 'dbName not found';
				$nMatch = preg_match ( "/define\('DB_USER', *'([^']+)' *\)/" , $strConfig , $aMatches);
				if ($nMatch) $strDBUser = $aMatches[1];  else echo 'dbUser not found';
				$nMatch = preg_match ( "/define\('DB_PASSWORD', *'([^']+)' *\)/" , $strConfig , $aMatches);
				if ($nMatch) $strPassword = $aMatches[1]; else echo 'dbPassword not found';
				$nMatch = preg_match ( "/define\('DB_HOST', *'([^']+)' *\)/" , $strConfig , $aMatches);
				if ($nMatch) $strHost = $aMatches[1]; else echo 'dbHost not found';
			}
			else {
				// look for processwire config file
				$configFile = "$this->HOME_DIR/site/config.php";
				if (file_exists($configFile)) {
					$strConfig = file_get_contents($configFile);
					$nMatch = preg_match ( "/config->dbName *= *'([^']+)'/" , $strConfig , $aMatches);
					if ($nMatch) $strDatabase = $aMatches[1]; else echo 'dbName not found';
					$strConfig = file_get_contents($configFile);
					$nMatch = preg_match ( "/config->dbUser *= *'([^']+)'/" , $strConfig , $aMatches);
					if ($nMatch) $strDBUser = $aMatches[1]; else echo 'dbUser not found';
					$strConfig = file_get_contents($configFile);
					$nMatch = preg_match ( "/config->dbPass *= *'([^']+)'/" , $strConfig , $aMatches);
					if ($nMatch) $strPassword = $aMatches[1]; else echo 'dbPassword not found';
					$strConfig = file_get_contents($configFile);
					$nMatch = preg_match ( "/config->dbHost *= *'([^']+)'/" , $strConfig , $aMatches);
					if ($nMatch) $strHost = $aMatches[1]; else echo 'dbHost not found';
				}
			}
			
			if ($strDatabase && $strDBUser && $strPassword && $strHost) {
				// echo @$strDatabase . ' ' . @$strDBUser . ' ' . @$strPassword . ' ' . @$strHost;
				// send to aws
				// sync site with aws
			}
			else die("Unable to read from config file: $configFile");
				
			//open database connection
			$this->db = new mysqli($strHost , $strDBUser , $strPassword , $strDatabase) ;
			if (mysqli_connect_errno()) die("Connect failed: " .  mysqli_connect_error());
			
			return $this->db;
		}
		
		public function FormatDateFromDir($dir) {
			$tmStartDate = strtotime(substr($dir,0,4) . '-' . substr($dir,4,2) . '-' . substr($dir,6,2));
			$tmEndDate = $tmStartDate + (6 * 24 * 60 * 60);
			return date('d-M-Y',$tmStartDate) . ' &mdash; ' . date('d-M-Y',$tmEndDate);
		}

		
			
	} // end class ChcsBackup

}
?>