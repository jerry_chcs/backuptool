<?php
session_start();
$verified = @$_SESSION['chcs']['verified'];

$BucketName = '';

function aws_config_exists() {
	global $BucketName;
	$bExists = false;
	$awsConfigFile = realpath('dbData/aws_config.php');
	if (file_exists($awsConfigFile)) {
		include($awsConfigFile);
		if (defined('SECRET')) $bExists = true;
		if (defined('BUCKET')) $BucketName = BUCKET;
	}
	return $bExists;
}

require 'Aws/aws-autoloader.php';

use Aws\Common\Enum\Region;
use Aws\Common\Aws;

$bError = 0;

$submit = @$_POST['formSubmit'];
if ($submit) {
	$BucketName = strtolower(@$_POST['BucketName']);
	// check for valid Bucket Name
	$aAllowedChars = 'abcdefghijklmnopqrstuvwxyz0123456789-.';
	$nBucketName = strlen($BucketName);
	$bError = (int)($nBucketName < 3);
	for ($i=0; $i<$nBucketName; $i++) if (strpos($aAllowedChars, $BucketName[$i]) === false) $bError = 1;
	if ($bError) $_SESSION['error'][] = 'Please enter a valid bucket name';
}
else {
	$BucketName = @$_GET['b'];
	$bError = @$_GET['e'];
}

$strPageName = ($verified ? 'Update AWS API Keys' : 'Set up new website');
$strPostName = ($verified ? 'Update AWS API Keys' : 'Set up new AWS API Keys');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $strPageName; ?></title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
// this is a "document ready" handler that fires after html is loaded but before window load
$(function(){ 

	$("#home").click(function(event){
		window.location = 'index.php';
	});


});
</script>
</head>

<body>
	<h2><?php echo $strPageName; ?></h2>
	
	<?php 
	$bConfigExists = aws_config_exists();
	
	if (!$verified && $bConfigExists) {
		?>
		<p>AWS Configuration file already exists</p>
		<p>Please select continue and then use the Restore->Update AWS API keys command to change API Keys</p>
		<form action="index.php" method="get">
		 <p><button type="submit">Continue</button></p>
		</form>
		<?php
	}
	else {

		if (!$submit || $bError) { 
			?>
			<form action="<?php basename(__FILE__); ?>" method="post" enctype="multipart/form-data">
			<?php 
			if ($bError && isset($_SESSION['error'])) {
				echo "<ul style=\"color:red; font-weight:bold;\">\n";
				foreach ($_SESSION['error'] as $msg) echo "<li>$msg</li>\n";
					echo "</ul>\n"; 
					unset($_SESSION['error']);
			}
			?>
			<p>
				<label for="BucketName">Bucket Name </label><br>
				<input type="text" name="BucketName" id="BucketName" size="50" maxlength="63" value="<?php echo $BucketName; ?>" style="text-transform:lowercase">
			</p>
			<div>
				<ul type="disc">
					<li>Bucket name should identify the site (e.g. thesitename.com)</li>
					<li>Bucket names must be at least 3 and no more than 63 characters long.</li>
					<li> Bucket names must contain only  lowercase letters, 
					numbers, and dashes.</li>
					<li>Bucket names must not be formatted as an IP address (e.g. 192.168.5.4)</li>
				</ul>
			</div>
			<p>
				<button type="button" id="home">Home</button>
				<button type="submit" name="formSubmit" value="submit"><?php echo $strPostName; ?></button>
			</p>
			</form>
			<?php
		}
		else {
		
			// AWS CHCS Account credentials
			define('API_KEY', 'AKIAIYVMVMHSJ7TXA77Q');
			define('SECRET', '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');
					
			try {
				// connect to AWS S3
				$awsConfig = array(
					'key' => API_KEY,
					'secret' => SECRET,
					'region' => Region::OREGON);
				// Create a service builder
				$aws = Aws::factory($awsConfig);
				// Get the client from the builder by namespace
				$client = $aws->get('S3');
			}
			catch(Exception $e) {
				die("<p>Unable Connect to AWS: " . $e->getMessage() . "</p>\n");
			}
			
			echo "<p>Connected to AWS</p>\n";
			
			// check if bucket exists
			try {
				$bBucketFound = 0;
				$blist = $client->listBuckets();
				foreach ($blist['Buckets'] as $bucket) {
					if ($bucket['Name'] == $BucketName) $bBucketFound = 1;
				}
				
				if (!$bBucketFound) {
					// create bucket
					$client->createBucket(array(
						'Bucket' => $BucketName,
						'LocationConstraint' => Region::OREGON
					));
					$bBucketFound = 1;
					echo "<p>Bucket '$BucketName' created</p>\n";
				}
				else echo "<p>Bucket '$BucketName' already exists</p>\n";
					
			}
			catch(Exception $e) {
				$bError = 1;
				$_SESSION['error'][] = "Unable to find / create bucket '$BucketName': (" . $e->getCode() . ') ' . $e->getMessage();
			}
			
			if (!$bError) {
				$bCreateNewUser = 0;
				try {
					$client = $aws->get('Iam');
					
					// see if user exists
					$user = $client->getUser(array(
								'UserName' => $BucketName
					));
					
					if (@$user['User']['UserName']) {
						// user exists: delete user keys so that new set can be created
						$aKeys = $client->listAccessKeys(array(
							'UserName' => $BucketName
						));		
						
						// iterate through keys - delete existing access keys
						if (isset($aKeys['AccessKeyMetadata']) && is_array($aKeys['AccessKeyMetadata'])) {
							foreach ($aKeys['AccessKeyMetadata'] as $KeyMetadata) {
							 echo "<p>Deleting access key: {$KeyMetadata['AccessKeyId']}</p>\n";
								$result = $client->deleteAccessKey(array(
									'UserName' => $BucketName,
									'AccessKeyId' => $KeyMetadata['AccessKeyId']
								));							
							}
						}
						
						// create new access keys
						echo "<p>Creating new access key</p>\n";
						$AccessKey = $client->createAccessKey(array(
							'UserName' => $BucketName
						));
						$AccessKeyId = $AccessKey['AccessKey']['AccessKeyId'];
						$SecretAccessKey = $AccessKey['AccessKey']['SecretAccessKey'];
														
					}
				}
				catch(Exception $e) {
					if ($e->getCode() == 0) {
						echo "<p>User '$BucketName' does not exist: (" . $e->getCode() . ') ' . $e->getMessage() . "</p>\n";
						$bCreateNewUser = 1;
					}
					else {
				  $_SESSION['error'][] = "Error: (" . $e->getCode() . ') ' . $e->getMessage();
						$bError = 1;
					}
				}
			}
			
			if (!$bError) {
				if ($bCreateNewUser) {
					try {
						echo "<p>Creating User '$BucketName'</p>\n";
						// create new user
						$user = $client->createUser(array(
							'UserName' => $BucketName
						));
						// create access keys
						$AccessKey = $client->createAccessKey(array(
							'UserName' => $BucketName
						));
						$AccessKeyId = $AccessKey['AccessKey']['AccessKeyId'];
						$SecretAccessKey = $AccessKey['AccessKey']['SecretAccessKey'];
					}
					catch(Exception $e) {
						$_SESSION['error'][] = "Could not create user '$BucketName': (" . $e->getCode() . ') ' . $e->getMessage();
						$bError = 1;
					}
				}
			}
		
		 if (!$bError) {
				$UserName = @$user['User']['UserName'];
				
				echo "<p>UserName: '$UserName' exists</p>\n";
				
				if ($UserName) {
					try {
						$groups = $client->listGroupsForUser(array(
							'UserName' => $UserName
						));
						
						// look for group in user's groups
						$GroupName = 'Website';
						$bGroupSet = 0;
						if (isset($groups['Groups']) && is_array($groups['Groups'])) {
							foreach ($groups['Groups'] as $Group) {
								if ($Group['GroupName'] == $GroupName) $bGroupSet = 1;
							}
						}
						
					}
					catch(Exception $e) {
						$_SESSION['error'][] = "Error getting user group: (" . $e->getCode() . ') ' . $e->getMessage();
						$bError = 1;
					}
				}
			}
			
			if (!$bError) {
				if (!$bGroupSet) {
					echo "<p>Adding user: '$UserName' to group '$GroupName'</p>\n";
					try {
						$client->addUserToGroup(array(
							'GroupName' => $GroupName,
							'UserName' => $UserName
						));
					}
					catch(Exception $e) {
						$_SESSION['error'][] = "Error adding user to group: (" . $e->getCode() . ') ' . $e->getMessage();
						$bError = 1;
					}
				}
			}
			
			// create aws_config.php file
			if (!$bError) {
				try {
					$file = new SplFileObject('dbData/aws_config.php','w');
					$file->fwrite("<?php\n" .
					"define('BUCKET','$BucketName');\n" . 
					"define('API_KEY', '$AccessKeyId');\n" . 
					"define('SECRET', '$SecretAccessKey');\n" . 
					"?>\n");
					unset($file);
					echo "<p>Created aws_config.php file</p>\n";
					echo "<p>AWS Access Key: $AccessKeyId</p>\n";
					echo "<p>AWS Secret Access Key: $SecretAccessKey</p>\n";
					
				}
				catch(Exception $e) {
					$_SESSION['error'][] = "Error saving aws config file: (" . $e->getCode() . ') ' . $e->getMessage();
					$bError = 1;
				}
			}
			
			if (!$bError) {
			?>
			<p>Follow instructions in help menu to run backup daily as a cron task</p>
			<form action="index.php" method="get">
			 <p><button type="submit">Continue</button></p>
			</form>
			<?php
			}
			else {
				?>
				<script type="text/javascript">
				 window.location = "<?php echo basename(__FILE__) . "?e=1&b=$BucketName"; ?>";
				</script>
				<?php
			}
		}
	}
?>

</body>
</html>